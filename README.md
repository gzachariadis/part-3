# ChatBot

This is a chat bot that replies to users if their message matches a certain regex or contains a word. It can be used to type commands and all other sorts of replies for example "Ποιοι παιζουν μετα" in message, fetch url or file content and reply.

### Advice to be fixed
<br>

Checking for PING anywhere in a string and replacing it could make you send shit to the server. Because this would work even if someone wrote "blah blah blah PING" to the chat. There's possibility someone could hijack something in here, for example, if you make a command for moderators that would add a new moderator (let's call it !mod <username>). You could basically create an account called "KappaPONG" and if you'd write "!mod KappaPING" to the chat, the bot would respond with "!mod KappaPONG", you don't have the permission to use that, but I suppose the bot would have the permission, so he would make user "KappaPONG" a moderator. It could apply to basically any command interacting with name.

You should check if the line is "PING :tmi.twitch.tv", then respond with "PONG :tmi.twitch.tv". Your workaround is short, but dangerous.